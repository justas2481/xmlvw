# XML Viewer (xmlvw V0.1.0)

This software was created due to the necessity to have a CLI tool
that could handle large XML files. It is written in Go
programming language.
It supports UTF-8 encoded XML files only.
It uses tokenization technique to analyze XML documents.
XML syntax errors are not tolerated, so if this tool meets an error,
it prints it out (line and error text) and exits.
It ignores errors in raw inspect mode so to let to inspect even those
large documents which are syntactically incorrect.
It is not heavily tested and yet has no Unit tests written
(this is because I need this tool ASAP and have no spare time to do so,
at least for now).
It is being used on Linux OS only.

## Main features

* Format huge XML files (as big as 4 GB and bigger ones);
* Search for textual content in XML tags and attributes;
* Inspect huge XMLs token by token and skip
    through certain amount of specific tags
    (this is useful when XML is so big that
    many editors lag or crash when trying to open it)
    or XML itself is laid down in one line to preserve space,
    but needs to be red / inspected;
* Count specified tags within XML document;
* Move through XML document as chunk of bytes even if it has invalid syntax;
* Validate XML document against XSD schema given.

## Commands

1. xmlvw fmt path/to/file.xml - formats XML document by indenting
    tags (depending on level) and content within tags.
    Adds new lines before each inner tag and after closing tags.
    Formatting process can handle large files,
    because it does not load file into memory,
    just reads it token by token as stream.
    Formatted XML file is created by following
    "formatted_{current_xml_filename_being_formatted.xml}" convention.
    Original file which is being formatted is left untouched.
    It does not preserve XML document type declaration directives (DTDs),
    but XML's prolog and comments are preserved.
    Namespaces are preserved in some form as well, but needs more testing.
1. xmlvw inspect path/to/file.xml - opens XML file given and
    stopps at the beginning.
    Then it waits for the instructions what to do and
    lets user to inspectan XML file.
    In some ways it works like a traditional debugger
    though without breakpoints and alike.
    It also lets to press return ("\r" / enter) for some commands / instructions
    to go on if tool is stopped at some token.
    Then old data that was entered is used to carry on.
1. xmlvw inspect-raw path/to/file.xml - lets to inspect XML
    file as if it would be in raw bytes (without any syntactical rules).
    In this mode, file is treeted as chunk of UTF-8 encoded bytes
    and some kind of pagination / buffering is used to
    skim through all the file contents.
    When you execute this command, it asks to enter the chunk size
    (which is the number of bytes you want to fit in single page).
    This way you walk through the file without loading it
    into memory and read an output in your terminal window.
    This is useful when XML has syntax errors and therefore tokenizing technique
    is actually impossible to do, because XML parsing fails with an error.
    UTF-8 multibyte Unicode points are preserved
    in a context so to treet them
    not as separate bytes, but in a correct form of Unicode points.
    You can walk forward and bacward through the file,
    as well as quickly search for some phrase within file.
    Speed of searching highly depends on chunk size that you pick.
    If you aren't sure, use 4096 bytes (which is default).
1. xmlvw xsd {schema.xsd} {file.xml} - checks if given XML is XSD compliant.
    This is done by using
    [xsdvalidate](https://github.com/terminalstatic/go-xsd-validate) package.
    Schema validation is performed by loading all file into memory,
    therefore it can not deal with large files.
1. xmlvw version - prints app version and exits.

### Commands when in inspect mode

* n - moves file pointer forward by one token, aka opening /closing tag,
    tag content or comment in the document.
* / - asks to enter a tag name and searches
    for a given tag opening in the document.
* \ - asks to enter a tag name and searches
    for a closing tag in the document by given tag name.
* ? - searches for an entered phrase in the document
    (understanding and following XML rules
    such as tags and attributes when searching).
    If you need to look for an empty tag, just press return ("\r" / enter)
    to search for an empty phrase.
* ?c - same as ?, but additionally it lets to enter the tag name and
    counts how many tags have been skipped until the search phrase has been found.
* ?t - same as ?c, but
    it searches for a phrase only within that specifically given tag.
    It counts how many such tags have been skipped
    until phrase in question has been found.
* c - asks for a tag name and counts how many given tags
    are found till the end of the document.
* s - asks to enter a tag name and amount of such tags to skip.
    Then it skipps that amount of named tags and
    stopps the pointer at the end of last one.

### Commands when in raw inspect mode

* n - reads next chunk of bytes given and
    shows the content in respect to Unicode points.
* p - moves file pointer backwards by the chunk size given
    and prints out the last chunk contents.
* ? - asks to enter the phrase to search for and moves through the document
    in chunks looking for the phrase given. If it finds the phrase,
    it prints out the newest page / chunk contents.
    If phrase in question overlaps between previous and current chunk,
    it prints a note about it to pay attention.
    Search / comparison is made byte by byte so it is case sensitive.

## Installation

There are 2 ways you can install this software.

### Compile from source code

The recommended way is to clone this repo and build everything from source.
What you need is to:
* [Download and install Go](https://go.dev/doc/install);
* Install everything that is required by xsdvalidate package.
    Basically if on Ubuntu:
    ```
    sudo apt-get install make-guile
    ```
    ```
    sudo apt-get install gcc
    ```
    ```
    curl -L ftp://xmlsoft.org/libxml2/LATEST_LIBXML2 -o ./LIBXML2_LATEST.tar.gz
    ```
    ```
    tar -xf ./LIBXML2_LATEST.tar.gz
    ```
    ```
    cd ./libxml2*
    ```
    ```
    ./configure --prefix=/usr  --enable-static --with-threads --with-history
    ```
    ```
    make
    ```
    ```
    sudo make install
    ```
    ```
    sudo apt-get install pkg-config
    ```
* Head over to repo you've cloned and build a standalone executable from there:
    ```
    go mod tidy
    ```
    ```
    go build .
    ```
    Then if you so desire,
    you can move it to /usr/local/bin directory to be able to call it from anywhere:
    ```
    sudo mv xmlvw /usr/local/bin/xmlvw
    ```
    To call it from the same directory, use:
    ```
    ./xmlvw (...)
    ```
    You can also add the directory path this tool is built in
    to your ~/.bashrc or /etc/profile.

### Download and install executable from Dropbox

1. Download a standalone executable via wget:
    ```
    wget -O xmlvw https://www.dropbox.com/s/1pmpbydao0qk7n1/xmlvw?dl=1
    ```
1. Give it permission to execute:
    ```
    sudo chmod +x xmlvw
    ```
1. If you so desire, move it to /usr/local/bin for easier access:
    ```
    sudo mv xmlvw /usr/local/bin/xmlvw
    ```
    By then you'll be able to call it from anywhere. For example:
    ```
    xmlvw fmt my_uggly_xml.xml
    ```

## Contributions

This source code was written quickly
and does not completely follow best practices
nor has tests.
If you wish, please contribute to this software by making a Merge request :).