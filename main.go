// xmlvw is the tool that helps to deal with big XML files,
// which can not be opened with regular editors due to their size.
package main

import (
	"bufio"
	"bytes"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strings"
	"unicode/utf8"

	xsdvalidate "github.com/terminalstatic/go-xsd-validate"
)

// File extensions.
const (
	xmlExt = ".xml"
	xsdExt = ".xsd"
)

// Output file prefix
const outputFilenamePrefix = "formatted_"

// Commands as strings mapped to handlers as functions
// or just useful to be used in other places.
const (
	cmdNext               = "n"
	cmdPrev               = "p"
	cmdSearchStartTag     = "/"
	cmdSearchEndTag       = "\\"
	cmdSearchContent      = "?"
	cmdSearchContentCount = "?c"
	cmdSearchContentInTag = "?t"
	cmdCountTags          = "c"
	cmdSkipTags           = "s"
	cmdFormat             = "fmt"
	cmdInspect            = "inspect"
	cmdInspectRaw         = "inspect-raw"
	cmdVersion            = "version"
	cmdXsd                = "xsd"
)

// Default chunk size in bytes to determine chunks when reading raw file.
const defaultChunkSize = 4096

// cmdHandler holds a handler responsible for resolving command.
type cmdHandler func(d *xml.Decoder, t xml.Token) (output string, err error)

// Valid commands.
var validCmds = map[string]cmdHandler{
	cmdNext:               next,
	cmdSearchStartTag:     searchForStartTag,
	cmdSearchEndTag:       searchForEndTag,
	cmdSearchContent:      searchForContent,
	cmdSearchContentCount: searchForContentWithCount,
	cmdSearchContentInTag: searchForContentInSpesificTag,
	cmdCountTags:          countTags,
	cmdSkipTags:           skipTags,
}

// cmdCach holds various user-entered cmd params for reuse
// if command continues process after already being called.
var cmdCache map[string]string

func init() {
	cmdCache = make(map[string]string)
}

func main() {
	if len(os.Args) == 3 && filepath.Ext(os.Args[2]) == xmlExt {
		if os.Args[1] == cmdFormat {
			if err := formatXML(outputFilenamePrefix, os.Args[2]); err != nil {
				fmt.Fprintf(os.Stderr, "Error: %s\n", err)
				os.Exit(1)
			}
			os.Exit(0)
		} else if os.Args[1] == cmdInspect {
			inspectXML()
		} else if os.Args[1] == cmdInspectRaw {
			inspectRawXML()
		} else {
			fmt.Println(
				"Unknown command or param. If you need help, please read the documentation.",
			)
		}
	} else if len(os.Args) == 2 && os.Args[1] == cmdVersion {
		fmt.Println("Version 0.1.0 made by Justinas Kilčiauskas.")
	} else if len(os.Args) == 4 &&
		os.Args[1] == cmdXsd &&
		filepath.Ext(os.Args[2]) == xsdExt &&
		filepath.Ext(os.Args[3]) == xmlExt {
		isXSDCompliant, err := checkXMLValidityByXSD(os.Args[2], os.Args[3])
		if err != nil {
			if !isXSDCompliant {
				fmt.Printf("Not XSD compliant: %s\n", err)
				os.Exit(0)
			}
			fmt.Fprintf(os.Stderr, "Error: %s\n", err)
			os.Exit(1)
		}
		fmt.Println("Congratulations, XML is XSD compliant!")
		os.Exit(0)
	} else {
		fmt.Println(
			"Unknown command. Head over to documentation to find out how to use this tool.",
		)
	}
}

func inspectXML() {
	fH, err := os.Open(os.Args[2])
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return
	}
	defer fH.Close()

	// Instantiate single XML decoder and set  opened xml file handle
	xmlDecoder := xml.NewDecoder(fH)

	// Read and process command from STDIN
	var cmdHandler cmdHandler
	for {
		fmt.Println(">")
		cmdHandler, err = readCmd(validCmds)
		if err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			continue
		}
		if err := resolveCmd(cmdHandler, xmlDecoder); err != nil {
			fmt.Fprintf(os.Stderr, "%v\n", err)
			return
		}
	}
}

// readCmd returns a valid command and its handler taken from possible ones.
func readCmd(validCmds map[string]cmdHandler) (handler cmdHandler, err error) {
	var cmd string
	fmt.Scanln(&cmd)
	// Check if return is pressed and prev cmd can be continued
	if cmd == "" && cmdCache["canBeContinued"] != "" {
		cmd = cmdCache["canBeContinued"]
	} else { // Clear cmd cache
		for key := range cmdCache {
			delete(cmdCache, key)
		}
	}

	var ok bool
	if handler, ok = validCmds[cmd]; !ok {
		return nil, fmt.Errorf("invalid command")
	}

	return handler, nil
}

// resolveCmd tears xml down token by token
// and resolves command by using its handler.
func resolveCmd(handler cmdHandler, d *xml.Decoder) error {
	var t xml.Token
	var err error
	t, err = d.Token()
	if err != nil {
		if err == io.EOF {
			fmt.Println("End of file.")
			os.Exit(0)
		}
		fmt.Fprintf(os.Stderr, "Token error: %v\n", err)
		os.Exit(1)
	}

	// Invoke the given command functionality
	output, err := handler(d, t)
	if err != nil {
		if err == io.EOF {
			return errors.New(output)
		}
		return err
	}
	if len(output) > 0 {
		fmt.Println(output)
	}

	return nil
}

func readTagname() string {
	var tagname string
	for {
		fmt.Println("Tag name: >")
		fmt.Scanln(&tagname)
		if len(tagname) < 1 {
			fmt.Println("Empty tag name!")
			continue
		}
		break
	}
	return tagname
}

func readPhrase() (string, error) {
	fmt.Println("Phrase: >")
	sc := bufio.NewScanner(os.Stdin)
	sc.Scan()
	if err := sc.Err(); err != nil {
		return "", fmt.Errorf("reading phrase: %w", err)
	}
	return sc.Text(), nil
}

func attrsStr(attr []xml.Attr) string {
	attrTotal := len(attr)
	attrStr := ""
	for i, attr := range attr {
		if attr.Name.Space != "" {
			attrStr += "xmlns:"
		}
		attrStr += attr.Name.Local + "=\"" + attr.Value + "\""
		if i+1 < attrTotal {
			attrStr += " "
		}
	}
	return attrStr
}

// Next goes to the next available token and returns its data.
func next(d *xml.Decoder, t xml.Token) (string, error) {
	var err error
	cmdCache["canBeContinued"] = cmdNext
outer:
	for {
	inner:
		switch t := t.(type) {
		case xml.StartElement:
			if len(t.Attr) > 0 {
				attrStr := attrsStr(t.Attr)
				return fmt.Sprintf("<%s %s>", t.Name.Local, attrStr), nil
			} else {
				return fmt.Sprintf("<%s>", t.Name.Local), nil
			}
		case xml.EndElement:
			return fmt.Sprintf("</%s>", t.Name.Local), nil
		case xml.CharData:
			xmlData := string(t)
			if strings.TrimSpace(xmlData) == "" {
				break inner
			}
			return fmt.Sprintf("%q", xmlData), nil
		case xml.Comment:
			return fmt.Sprintf("<!-- %s -->", string(t)), nil
		case xml.ProcInst:
			return fmt.Sprintf("<?%s %s?>", t.Target, string(t.Inst)), nil
		case xml.Directive:
			return fmt.Sprintf("<!%s>", string(t)), nil
		default:
			break outer // Should not be reached under normal conditions
		}

		t, err = d.Token()
		if err != nil {
			if err == io.EOF {
				fmt.Println("End of file.")
				os.Exit(0)
			}
			fmt.Fprintf(os.Stderr, "Token error: %v\n", err)
			os.Exit(1)
		}
	}

	return "", nil
}

// searchForStartTag searches for a nearest tag which fits the entered tag name.
func searchForStartTag(d *xml.Decoder, t xml.Token) (string, error) {
	var tagname string
	if cmdCache["oldTagname"] != "" {
		tagname = cmdCache["oldTagname"]
	} else {
		tagname = readTagname()
	}

	cmdCache["oldTagname"] = tagname
	cmdCache["canBeContinued"] = cmdSearchStartTag
	for {
		var err error
		switch t := t.(type) {
		case xml.StartElement:
			if tagname == t.Name.Local {
				return fmt.Sprintf("<%v>", string(t.Name.Local)), nil
			}
		}
		t, err = d.Token()
		if err != nil {
			if err == io.EOF {
				return "Tag that you specified haven't been found.", io.EOF
			}
			return "", err
		}
	}
}

// searchForEndTag searches for a tag that ends as entered tag name.
func searchForEndTag(d *xml.Decoder, t xml.Token) (string, error) {
	var tagname string
	if cmdCache["oldTagname"] != "" {
		tagname = cmdCache["oldTagname"]
	} else {
		tagname = readTagname()
	}

	cmdCache["oldTagname"] = tagname
	cmdCache["canBeContinued"] = cmdSearchEndTag
	for {
		var err error
		switch t := t.(type) {
		case xml.EndElement:
			if tagname == t.Name.Local {
				return fmt.Sprintf("</%v>", string(t.Name.Local)), nil
			}
		}
		t, err = d.Token()
		if err != nil {
			if err == io.EOF {
				return "Tag that you specified haven't been found.", io.EOF
			}
			return "", err
		}
	}
}

// searchForContent searches for a content in XML document.
//
// Content can be empty as well as present.
//
// It searches across all the document inside tags and attributes
// and reads entire line from stdin as search phrase.
func searchForContent(d *xml.Decoder, t xml.Token) (string, error) {
	var phrase string
	var err error
	if _, ok := cmdCache["oldContent"]; ok {
		phrase = cmdCache["oldContent"]
	} else {
		phrase, err = readPhrase()
		if err != nil {
			return "", fmt.Errorf("reading phrase: %w", err)
		}
	}

	cmdCache["oldContent"] = phrase
	cmdCache["canBeContinued"] = cmdSearchContent
	tagNoContent := true // To check if tag is closed without having content
	for {
		var err error
		switch t := t.(type) {
		case xml.CharData:
			xmlData := string(t)
			if phrase == "" && strings.TrimSpace(xmlData) == "" {
				break
			}
			tagNoContent = false
			if phrase != "" && strings.Contains(xmlData, phrase) {
				return fmt.Sprintf("%q", xmlData), nil
			}
		case xml.StartElement:
			if len(t.Attr) > 0 {
				for _, attr := range t.Attr {
					if (phrase != "" && strings.Contains(attr.Value, phrase)) ||
						(phrase == "" && strings.TrimSpace(attr.Value) == "") {
						attrStr := attrsStr(t.Attr)
						return fmt.Sprintf("<%s %s>", t.Name.Local, attrStr), nil
					}
				}
			}
			tagNoContent = true
		case xml.EndElement:
			if phrase == "" && tagNoContent {
				return fmt.Sprintf("</%s>\n", t.Name.Local), nil
			}
		}
		t, err = d.Token()
		if err != nil {
			if err == io.EOF {
				return "Phrase that you specified haven't been found.", io.EOF
			}
			return "", err
		}
	}
}

// searchForContentWithCount searches for a content
// and additionally prints a counter by a tag specified via stdin by the user.
func searchForContentWithCount(d *xml.Decoder, t xml.Token) (string, error) {
	var phrase string
	var err error
	if _, ok := cmdCache["oldContent"]; ok {
		phrase = cmdCache["oldContent"]
	} else {
		phrase, err = readPhrase()
		if err != nil {
			return "", fmt.Errorf("reading phrase: %w", err)
		}
	}

	var tagname string
	if cmdCache["oldTagname"] != "" {
		tagname = cmdCache["oldTagname"]
	} else {
		tagname = readTagname()
	}

	var tagCounter uint64 = 0
	cmdCache["oldContent"] = phrase
	cmdCache["oldTagname"] = tagname
	cmdCache["canBeContinued"] = cmdSearchContentCount
	tagNoContent := true // To check if tag is closed without having content
	for {
		var err error
		switch t := t.(type) {
		case xml.StartElement:
			if t.Name.Local == tagname {
				tagCounter++
			}
			tagNoContent = true
			if len(t.Attr) > 0 {
				for _, attr := range t.Attr {
					if (phrase != "" && strings.Contains(attr.Value, phrase)) ||
						(phrase == "" && strings.TrimSpace(attr.Value) == "") {
						attrStr := attrsStr(t.Attr)
						return fmt.Sprintf(
							"Counted %d <%s>, <%s %s>", tagCounter, tagname, t.Name.Local, attrStr,
						), nil
					}
				}
			}
		case xml.CharData:
			xmlData := string(t)
			if phrase == "" && strings.TrimSpace(xmlData) == "" {
				break
			}
			tagNoContent = false
			if phrase != "" && strings.Contains(xmlData, phrase) {
				return fmt.Sprintf(
					"Counted %d <%s>, %q", tagCounter, tagname, xmlData,
				), nil
			}
		case xml.EndElement:
			if phrase == "" && tagNoContent {
				return fmt.Sprintf(
					"Counted %d <%s>, </%s>", tagCounter, tagname, t.Name.Local,
				), nil
			}
		}
		t, err = d.Token()
		if err != nil {
			if err == io.EOF {
				return "Phrase that you specified haven't been found.", io.EOF
			}
			return "", err
		}
	}
}

// searchForContentInSpecificTag searches for a content
// in a given tag (attributes included).
func searchForContentInSpesificTag(
	d *xml.Decoder,
	t xml.Token,
) (string, error) {
	var phrase string
	var err error
	if _, ok := cmdCache["oldContent"]; ok {
		phrase = cmdCache["oldContent"]
	} else {
		phrase, err = readPhrase()
		if err != nil {
			return "", fmt.Errorf("reading phrase: %w", err)
		}
	}

	var tagname string
	if cmdCache["oldTagname"] != "" {
		tagname = cmdCache["oldTagname"]
	} else {
		tagname = readTagname()
	}

	cmdCache["oldContent"] = phrase
	cmdCache["oldTagname"] = tagname
	cmdCache["canBeContinued"] = cmdSearchContentInTag
	tagNoContent := true // To check if tag is closed without having content
	currentTagname := ""
	var tagCounter uint64 = 0
	for {
		var err error
		switch t := t.(type) {
		case xml.StartElement:
			currentTagname = t.Name.Local
			tagNoContent = true
			if t.Name.Local == tagname {
				tagCounter++
				if len(t.Attr) > 0 {
					for _, attr := range t.Attr {
						if (phrase != "" && strings.Contains(attr.Value, phrase)) ||
							(phrase == "" && strings.TrimSpace(attr.Value) == "") {
							attrStr := attrsStr(t.Attr)
							return fmt.Sprintf(
								"Counted %d <%s>, <%s %s>",
								tagCounter,
								tagname,
								t.Name.Local,
								attrStr,
							), nil
						}
					}
				}
			}
		case xml.CharData:
			tagNoContent = false
			if currentTagname == tagname {
				xmlData := string(t)
				if phrase == "" && strings.TrimSpace(xmlData) == "" {
					break
				}
				if phrase != "" && strings.Contains(xmlData, phrase) {
					return fmt.Sprintf("Counted %d <%s>, %q", tagCounter, tagname, xmlData), nil
				}
			}
		case xml.EndElement:
			if phrase == "" && tagNoContent {
				return fmt.Sprintf(
					"Counted %d <%s>, </%s>",
					tagCounter,
					tagname,
					t.Name.Local,
				), nil
			}
		}
		t, err = d.Token()
		if err != nil {
			if err == io.EOF {
				return "Phrase that you specified haven't been found.", io.EOF
			}
			return "", err
		}
	}
}

// countTags counts a specified tag occurance within XML file.
func countTags(d *xml.Decoder, t xml.Token) (string, error) {
	tagname := readTagname()
	var tagsTotal uint = 0
	for {
		var err error
		switch t := t.(type) {
		case xml.StartElement:
			if tagname == t.Name.Local {
				tagsTotal += 1
			}
		}
		t, err = d.Token()
		if err != nil {
			if err == io.EOF {
				return fmt.Sprintf("%d <%s> in total.", tagsTotal, tagname), io.EOF
			}
			return "", err
		}
	}
}

// skipTags allows to skip specified tag for certain number of times.
func skipTags(d *xml.Decoder, t xml.Token) (string, error) {
	tagname := readTagname()
	var skips uint
	for {
		fmt.Println("How many tags you want to skip: >")
		fmt.Scanf("%d", &skips)
		if skips < 1 {
			fmt.Println("You should specify an amount of tags to be skipped!")
			continue
		}
		break
	}

	var tagsTotal uint = 0
	for {
		var err error
		switch t := t.(type) {
		case xml.EndElement:
			if tagname == t.Name.Local {
				tagsTotal += 1
			}
		}
		if tagsTotal >= skips {
			return fmt.Sprintf("Skipped %d <%s> tags in total.", tagsTotal, tagname), nil
		}
		t, err = d.Token()
		if err != nil {
			if err == io.EOF {
				return fmt.Sprintf("Skipped %d <%s> tags in total.", tagsTotal, tagname), io.EOF
			}
			return "", err
		}
	}
}

// FormatXML indents XML tags by spaces depending on indentation level.
// It adds "\n" when inner tag immediately follows outer tag.
func formatXML(outFlPrefix, filename string) error {
	flrStat, err := os.Stat(filename)
	if err != nil {
		return fmt.Errorf("file stat: %w", err)
	}
	flrSize := flrStat.Size()
	flr, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("opening xml file for reading: %w", err)
	}
	defer flr.Close()

	flw, err := os.Create(outFlPrefix + filepath.Base(filename))
	if err != nil {
		return fmt.Errorf("creating xml file for writing: %w", err)
	}
	defer flw.Close()

	xmlDec := xml.NewDecoder(flr)
	var indentLevel int
	var output string
	var percentCurrent int64 = -1
	const buffLimit = 4096
	for {
		tok, err := xmlDec.Token()
		if err != nil {
			if err == io.EOF {
				break
			}
			return fmt.Errorf("xml token: %w", err)
		}

		percent := xmlDec.InputOffset() * 100 / flrSize
		if percent == 100 ||
			(percent-percentCurrent > 9 || percentCurrent == -1) {
			percentCurrent = percent
			fmt.Println("Formatting file:", percentCurrent, "%...")
		}

		switch t := tok.(type) {
		case xml.StartElement:
			if len(t.Attr) > 0 {
				attrStr := attrsStr(t.Attr)
				output += indentValue(
					fmt.Sprintf("<%s %s>", t.Name.Local, attrStr), indentLevel,
				)
			} else {
				output += indentValue(fmt.Sprintf("<%s>", t.Name.Local), indentLevel)
			}
			indentLevel++
		case xml.EndElement:
			indentLevel--
			output += indentValue(fmt.Sprintf("</%s>", t.Name.Local), indentLevel)
		case xml.CharData:
			xmlData := strings.TrimSpace(string(t))
			if xmlData == "" { // Get rid of empty data
				continue
			}
			xmlData = "<![CDATA[" + xmlData + "]]>"
			output += indentValue(xmlData, indentLevel)
		case xml.ProcInst:
			output += fmt.Sprintf("<?%s %s?>\n", t.Target, string(t.Inst))
		case xml.Comment:
			output += indentValue(strings.TrimSpace("<!--"+string(t)+"-->"), indentLevel)
		}

		if len(output) < buffLimit && xmlDec.InputOffset() < flrSize {
			continue
		}
		if _, err = flw.Write([]byte(output)); err != nil {
			return fmt.Errorf("writing xml data to file: %w", err)
		}
		output = ""
	}

	log.Println(
		"Format complete. Your new file is saved in",
		outFlPrefix+filepath.Base(filename),
	)
	return nil
}

func indentValue(v string, indentLevel int) string {
	indent := ""
	for i := 0; i < indentLevel; i++ {
		indent += "  "
	}
	return fmt.Sprintf("%s%s\n", indent, v)
}

func checkXMLValidityByXSD(
	xsdFilePath string,
	xmlFilePath string,
) (isXSDCompliant bool, err error) {
	if err := xsdvalidate.Init(); err != nil {
		return true, fmt.Errorf("initializing libxml2: %w", err)
	}
	defer xsdvalidate.Cleanup()

	xsdhandler, err := xsdvalidate.NewXsdHandlerUrl(
		xsdFilePath,
		xsdvalidate.ParsErrDefault,
	)
	if err != nil {
		return true, fmt.Errorf("initializing xsd handler: %w", err)
	}
	defer xsdhandler.Free()

	xmlContent, err := os.ReadFile(xmlFilePath)
	if err != nil {
		return true, fmt.Errorf("reading xml file: %w", err)
	}
	if err := xsdhandler.ValidateMem(
		xmlContent,
		xsdvalidate.ValidErrDefault,
	); err != nil {
		switch err := err.(type) {
		case xsdvalidate.ValidationError:
			fmt.Printf(
				"Error in line: %d\nMessage: %s\n",
				err.Errors[0].Line,
				err.Errors[0].Message,
			)
			return true, err
		default:
			return false, fmt.Errorf("default validation case: %w", err)
		}
	}

	return false, nil // Pass xsd
}

func inspectRawXML() {
	fH, err := os.Open(os.Args[2])
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		return
	}
	defer fH.Close()

	// Read chunk from stdin
	var chunk int
	for {
		fmt.Printf(
			"Read bytes per chunk (<=0 = default (%d bytes)): >",
			defaultChunkSize,
		)
		if _, err := fmt.Scan(&chunk); err != nil {
			fmt.Printf("Bad input: %s\n", err)
			continue
		}
		if chunk <= 0 {
			chunk = defaultChunkSize
		}
		break
	}

	// Read file buy chunks
	rdr := bufio.NewReader(fH)
	var prevBuff []byte
	var cmd string
	var bytesMoved int64 = 0
	for {
		fmt.Println(">")
		fmt.Scanln(&cmd)
		switch cmd {
		case cmdNext:
			b, data, err := readRawBytesAsChunk(rdr, chunk)
			if err != nil && err != io.EOF {
				fmt.Fprintf(os.Stderr, "Error: %s\n", err)
				os.Exit(1)
			}
			fmt.Printf("%s\n", data)
			if err == io.EOF {
				fmt.Println("End of file.")
				return
			}
			bytesMoved += int64(b)
			prevBuff = append(make([]byte, 0, b), data...)
		case cmdPrev:
			if bytesMoved <= int64(chunk) {
				fmt.Fprintln(os.Stderr, "There is nowhere else to go bacward.")
				break
			}
			bytesMoved -= int64(chunk * 2)
			if bytesMoved < 0 {
				bytesMoved = 0
			}
			if _, err := fH.Seek(bytesMoved, io.SeekStart); err != nil {
				fmt.Fprintf(os.Stderr, "Error when seeking file: %s\n", err)
				os.Exit(1)
			}
			rdr.Reset(fH)
			b, data, err := readRawBytesAsChunk(rdr, chunk)
			if err != nil && err != io.EOF {
				fmt.Fprintf(os.Stderr, "Error: %s\n", err)
				os.Exit(1)
			}
			fmt.Printf("%s\n", data)
			if err == io.EOF {
				fmt.Println("End of file.")
				return
			}
			bytesMoved += int64(b)
			prevBuff = append(make([]byte, 0, b), data...)
		case cmdSearchContent:
			var phrase string
			var err error
			for {
				phrase, err = readPhrase()
				if err != nil {
					fmt.Fprintf(os.Stderr, "Error: %s\n", err)
					continue
				}
				if phrase == "" { // Makes no sense in raw files to search for empty
					fmt.Println("Phrase can not be empty!")
					continue
				}
				if len(phrase) > chunk {
					fmt.Println(
						"Phrase can not be longer than the given buffer to read file by chunks!",
					)
					continue
				}
				break
			}

			// Read file and search
			var data []byte
			var b int
			phraseB := []byte(phrase)
			for {
				b, data, err = readRawBytesAsChunk(rdr, chunk)
				if err != nil && err != io.EOF {
					fmt.Fprintf(os.Stderr, "Error: %s\n", err)
					os.Exit(1)
				}
				bytesMoved += int64(b)
				if idx := bytes.LastIndex(append(prevBuff, data...), phraseB); idx > -1 {
					fmt.Println("Phrase has been found.")
					if idx+1 < len(prevBuff) {
						fmt.Println(
							"Note: given chunk overlaps for the phrase.",
						)
						if _, err := fH.Seek(bytesMoved-int64(b), io.SeekStart); err != nil {
							fmt.Fprintf(os.Stderr, "Error when seeking file: %s\n", err)
							os.Exit(1)
						}
						copy(data, prevBuff)
						prevBuff = prevBuff[0:0]
						rdr.Reset(fH)
						bytesMoved -= int64(b)
					}
					fmt.Print("Press enter / return to review it.")
					fmt.Scanln()
					fmt.Println(string(data))
					break
				}
				if err == io.EOF {
					fmt.Println("Phrase that you specified haven't been found.")
					return
				}
				prevBuff = append(make([]byte, 0, b), data...)
			}
		default:
			fmt.Println("Unknown command!")
		}
	}
}

// readRawBytesAsChunk reads at most chunkSize
// bytes from a reader and returns slice of bytes read
// as well as number of bytes read.
// It can as well return chunkSize+(<=4) bytes
// to preserve UTF-8 encoded Unicode points.
//
// If there are less bytes to read than chunkSize,
// it returns bytes read.
//
// It ensures that UTF-8 encoded Unicode points aren't damaged
// wwhich can happen if codepoint consists of more than one byte.
// It does so by checking where exactly new rune starts
// and unreads that last byte.
//
// It respects io.EOF and other STD errors as usual.
//
// ChunkSize must be >= 8 bytes
// and should always divide by 4 without a remainder,
// aka remainder should be equal to 0.
// This is important so to keep UTF-8 encoding handling simpler,
// because maximum 4 bytes can be aquired per rune.
func readRawBytesAsChunk(r *bufio.Reader, chunkSize int) (int, []byte, error) {
	if chunkSize < 8 {
		return 0, []byte{}, errors.New("chunkSize must be greater or equal to 4")
	}
	if chunkSize%4 != 0 {
		return 0, []byte{}, errors.New("chunkSize must divide by 4 without a remainder")
	}
	data := make([]byte, chunkSize)
	size, err := r.Read(data)
	if err == io.EOF {
		return size, data, io.EOF
	}
	if err != nil {
		return size, []byte{}, fmt.Errorf("reading bytes from reader: %w", err)
	}

	// Consume all the bytes which aren't the start of rune
	// and if it is the start of a rune, unread that last byte.
	for {
		b, err := r.ReadByte()
		if err == io.EOF {
			return 0, data, io.EOF
		}
		if err != nil {
			return 0, data, fmt.Errorf("reading single byte: %w", err)
		}
		if utf8.RuneStart(b) {
			r.UnreadByte()
			break
		}
		data = append(data, b)
	}

	return size, data, nil
}
